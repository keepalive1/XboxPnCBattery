# XboxPnCBattery



## What ist this project ?

This is an attempt at reverse engineering all HW required to build the rechargeable battery packs for Xbox 360 controllers.
This first commit contains all KiCAD files for the (honestly pretty simple) PCB that's inside the battery pack.

## ToDo
 Further components required are the 3D design files to 3D print the casing for the battery pack and an image of the EEPROM that's part of this battery pack.
Also a BOM is required to make shopping for the 10 or so components easier.
I'm not sure if it would be okay to just upload an image of the contents of the EEPROM in this repo, but I'm just going to be cautious and encourage you to look somewhere else for that. Or buy a used/broken battery and 
extract the image from it's EEPROM.

-Add 3D files 

-Upgrade EEPROM instructions

-Create BOM

-Build a prototype to verify all working well

